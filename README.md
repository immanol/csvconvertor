## Structure
* DataParser  => Parsing file lines to case class
* HtmlWriter  => Writing case classes as html
* FileHandler => Wrap DataParser and HtmlWriter to build a component
* instances   => this package contains different implementations for our parser and writer
* interfaces  => expose functionality of DataParser and HtmlWriter to outside using utility methods
* syntax      =>  expose functionality of DataParser and HtmlWriter to outside using extention methods
## How to Run
This is a SBT application wit test and integration test.

Test: sbt test
ITTest: sbt IntegrationTest/test
Run: sbt run

It automatically read two file from resources and write the result on resources/output
