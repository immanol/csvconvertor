name := "BETest"

version := "0.1"

scalaVersion := "2.12.7"


lazy val coreDependencies = Seq(
  "org.scalactic" %% "scalactic" % "3.0.8",
  "org.typelevel" %% "cats-core" % "2.0.0-RC1",
  "org.typelevel" %% "cats-effect" % "2.0.0-RC1"
)
lazy val testDependencies = Seq(
  "org.scalatest" %% "scalatest" % "3.0.8",
  "org.seleniumhq.selenium" % "selenium-java" % "2.35.0"
).map(_ % "it,test")

lazy val root = project
  .in(file("."))
  .configs(IntegrationTest)
  .settings(
    Defaults.itSettings,
    libraryDependencies ++= coreDependencies ++ testDependencies
  )
  .settings(mainClass in (Compile, run) := Some("ir.immanol.app.BeApplication"))
  .settings(
    scalacOptions ++= List(
      "-encoding",
      "UTF-8",
      "-target:jvm-1.8",
      "-language:existentials",
      "-language:higherKinds",
      "-language:postfixOps",
      "-unchecked",
      "-Yno-adapted-args",
      "-Ywarn-dead-code",
      "-Ywarn-inaccessible",
      "-Ylog-classpath",
      "-Ywarn-unused",
      "-Ywarn-unused-import",
      "-language:implicitConversions",
      "-unchecked",
      "-deprecation",
      "-feature",
      "-Ypartial-unification",
      "-language:postfixOps",
    )
  )
