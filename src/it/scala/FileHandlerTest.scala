import java.io.File
import java.nio.file.FileSystems

import ir.immanol.models.Info
import ir.immanol.structure.fileHandlers.FileHandler
import org.scalatest.{FlatSpec, Matchers}
import org.scalatestplus.selenium.HtmlUnit

import scala.io.Codec
import scala.util.Random

class FileHandlerTest extends FlatSpec with Matchers with HtmlUnit{

  behavior of "FileHandler"

  it should "read and csv file and return a Right[Seq[Info]]" in {
    import ir.immanol.structure.instances.parser.ParserInstances.InfoCsvDataParser
    import ir.immanol.structure.instances.html.HtmlWriterInstances.InfoSimpleHtmlWriter
    implicit val codec = Codec("ISO-8859-1")

    val source = FileSystems.getDefault.getPath("src/main/resources/Workbook2.csv")
    new FileHandler[Info]()(InfoCsvDataParser, InfoSimpleHtmlWriter, codec).read(source, true).value.unsafeRunSync() match{
      case Right(d) => {
        d should have size 7
        succeed
      }
      case Left(e) => fail(e.getMessage)
    }
  }
  it should "read and prn file and return a Right[Seq[Info]]" in {
    import ir.immanol.structure.instances.parser.ParserInstances.InfoPrnDataParser
    import ir.immanol.structure.instances.html.HtmlWriterInstances.InfoSimpleHtmlWriter
    implicit val codec = Codec("ISO-8859-1")

    val source = FileSystems.getDefault.getPath("src/main/resources/Workbook2.prn")
    new FileHandler[Info]()(InfoPrnDataParser, InfoSimpleHtmlWriter, codec).read(source, true).value.unsafeRunSync() match{
      case Right(d) => {
        d should have size 7
        succeed
      }
      case Left(e) => fail(e.getMessage)
    }
  }

  it should "read and csv file and save it as html" in {
    import ir.immanol.structure.instances.parser.ParserInstances.InfoCsvDataParser
    import ir.immanol.structure.instances.html.HtmlWriterInstances.InfoSimpleHtmlWriter
    implicit val codec = Codec("ISO-8859-1")

    val source = FileSystems.getDefault.getPath("src/main/resources/Workbook2.csv")
    val destFile = File.createTempFile("csv-html", s"${Random.nextInt(1000)}.htm")
    new FileHandler[Info]()(InfoCsvDataParser, InfoSimpleHtmlWriter, codec).saveAsHtml(source, destFile.toPath, true).unsafeRunSync() match{
      case Right(_) => {
        println(destFile.toString)
        go to (s"file://${destFile.toString}")
        val rows = findAll(tagName("tr"))
       rows should have size 8
        destFile.delete()
        succeed
      }
      case Left(e) => fail(e.getMessage)
    }
  }
}
