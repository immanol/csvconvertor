package ir.immanol.app


import java.nio.file.FileSystems

import ir.immanol.models.Info
import ir.immanol.structure.fileHandlers.FileHandler

import scala.io.Codec

object BeApplication extends App {

  println("Application started ...")

  import ir.immanol.structure.instances.parser.ParserInstances.InfoCsvDataParser
  import ir.immanol.structure.instances.parser.ParserInstances.InfoPrnDataParser
  import ir.immanol.structure.instances.html.HtmlWriterInstances.InfoSimpleHtmlWriter

  implicit val codec = Codec("ISO-8859-1")

  val csvSource = FileSystems.getDefault.getPath("src/main/resources/Workbook2.csv")
  val csvDest    = FileSystems.getDefault.getPath("src/main/resources/output/Workbook2-csv.html")

  new FileHandler[Info]()(InfoCsvDataParser, InfoSimpleHtmlWriter, codec).saveAsHtml(csvSource, csvDest, true).unsafeRunSync() match{
    case Right(_) => println("Run Successfully")
    case Left(e) => println(s" Error in application ${e.getMessage}")
  }

  val prnSource = FileSystems.getDefault.getPath("src/main/resources/Workbook2.prn")
  val prnDest   = FileSystems.getDefault.getPath("src/main/resources/output/Workbook2-prn.html")
  new FileHandler[Info]()(InfoPrnDataParser, InfoSimpleHtmlWriter, codec).saveAsHtml(prnSource, prnDest, true).unsafeRunSync() match{
    case Right(_) => println("Done Successfully")
    case Left(e) => println(s" Error in application ${e.getMessage}")
  }

  println("Application stopped ...")
}
