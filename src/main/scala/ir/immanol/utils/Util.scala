package ir.immanol.utils

import scala.util.matching.Regex

object Util {

  /**
    * Combine multiple regex to build a bigger regex
    * @param args list of regexs
    * @param separator operation for merging regex
    * @return
    */
  def combineRegex(args: Regex*)(separator: String) = {
    args.map(_.pattern).mkString(separator).r
  }

  /**
    * Flatten a Seq[Either[A, B] to Either[A, Seq[B]]
    * @return Either[A, Seq[B]]
    */
  def flattenEither[A, B](x: Seq[Either[A, B]]): Either[A, Seq[B]] =
    (x foldRight (Right(Nil): Either[A, Seq[B]])) { (e, acc) =>
      for (xs <- acc.right; x <- e.right) yield (x +: xs)
    }
}
