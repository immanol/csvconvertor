package ir.immanol.models

import java.time.LocalDate

final case class Info(name: String, address: String, postcode: String,
                      phone: String, creditLimit: Double, birthday: LocalDate)
