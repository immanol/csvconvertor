package ir.immanol.structure.interfaces.html

import java.io.PrintWriter
import java.nio.file.Path

import cats.data.EitherT
import cats.effect.IO
import ir.immanol.structure.htmlWriters.HtmlWriter

import scala.util.Try

/**
  * Our HtmlWriter and DataParser instances needs some interface ro expose their functionality to outside.
  * This class is in charge of this.
  */
object Html {
  def toHtml[A](value: A)(implicit w: HtmlWriter[A]): String = {
    w.write(value)
  }
  def toHtml[A](values: Seq[A])(implicit w: HtmlWriter[A]): String = {
    values.map(value => w.write(value)).mkString("")
  }

  /**
    * Side effects is wrap in an IO Monad
    */
  def htmlAsFile[A](values: Seq[A], path: Path)(implicit w: HtmlWriter[A]): EitherT[IO, Throwable, Unit] ={
    val start = """<html><body><table>"""
    val end = """</table></body></html>"""
    EitherT {
      IO {
        Try {
          val file = new PrintWriter(path.toFile)
          file.write(start)
          file.write(w.writeHeader)
          file.write(toHtml(values))
          file.write(end)
          file.close()
        }.toEither
      }
    }
  }
}
