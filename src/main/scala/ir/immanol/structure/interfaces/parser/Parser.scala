package ir.immanol.structure.interfaces.parser

import ir.immanol.structure.parsers.DataParser

/**
  * Our DataParser and DataParser instances needs some interface ro expose their functionality to outside.
  * * Object Parser is in charge of this.
  */
object Parser {
    def toModel[A](value: String)(implicit p: DataParser[A]): Either[Throwable, A] = {
      p.parse(value)
    }
}
