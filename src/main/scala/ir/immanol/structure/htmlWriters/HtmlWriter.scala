package ir.immanol.structure.htmlWriters


/**
  * This trait is in charge of converting a case class to html
  * @tparam A
  */
trait HtmlWriter[A] {
  def write(value: A): String
  def writeHeader: String
}
