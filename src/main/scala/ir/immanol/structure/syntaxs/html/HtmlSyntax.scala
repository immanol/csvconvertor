package ir.immanol.structure.syntaxs.html

import java.nio.file.Path

import cats.data.EitherT
import cats.effect.IO
import ir.immanol.structure.htmlWriters.HtmlWriter
import ir.immanol.structure.interfaces.html.Html

/**
  * Our [[HtmlWriter]] instances needs some interface ro expose their functionality to outside.
  * [[HtmlSyntax]] is in charge of exposing functionality in terms of extention methods.
  */
object HtmlSyntax {

  implicit class HtmlWriterOps[A](value: A){
    def toHtml(implicit w: HtmlWriter[A]): String = w.write(value)
  }

  implicit class HtmlWriterSeqOps[A](values: Seq[A]){
    def toHtml(implicit w: HtmlWriter[A]): String = {
      values.map(value => w.write(value)).mkString("")
    }
  }
  implicit class htmlAsFileOps[A](values: Seq[A]){
    def htmlAsFile(path: Path)(implicit w: HtmlWriter[A]): EitherT[IO, Throwable, Unit] = Html.htmlAsFile(values, path)
  }
}
