package ir.immanol.structure.syntaxs.parser

import ir.immanol.structure.parsers.DataParser

/**
  * Our [[DataParser]] instances needs some interface ro expose their functionality to outside.
  * [[ParserSyntax]] is in charge of exposing functionality in terms of extention methods.
  */
object ParserSyntax {

  implicit class DataParserOps[A](value: String) {
    def toModel(implicit p: DataParser[A]): Either[Throwable, A] = p.parse(value)
  }
}
