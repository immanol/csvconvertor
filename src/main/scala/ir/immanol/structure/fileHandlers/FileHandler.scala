package ir.immanol.structure.fileHandlers

import java.nio.charset.CodingErrorAction
import java.nio.file.Path
import cats.data.EitherT
import cats.effect.IO
import ir.immanol.structure.htmlWriters.HtmlWriter
import ir.immanol.structure.interfaces.html.Html
import ir.immanol.structure.syntaxs.parser.ParserSyntax._
import ir.immanol.structure.parsers.DataParser
import ir.immanol.utils.Util._

import scala.io.{Codec, Source}
import scala.util.Try

/**
  * [[FileHandler]] is in charge of wrap [[DataParser]]s and [[HtmlWriter]]s to read data from file, parse each line
  * and if it is needed writing it's equivalent html to a file.
  */
class FileHandler[A](implicit parser: DataParser[A], htmlWriter: HtmlWriter[A], codec: Codec){
  codec.onMalformedInput(CodingErrorAction.REPLACE)
  codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

   def read(source: Path, hasHeader: Boolean): EitherT[IO, Throwable, Seq[A]] = {
     EitherT {
       IO(Source.fromFile(source.toFile)).bracket{
         in => IO{
           Try(
             flattenEither(in.getLines().drop(if (hasHeader) 1 else 0).map(_.toModel).toSeq)
           ).toEither.joinRight
         }
       }{ in => IO(in.close())}
     }
  }

  def saveAsHtml(source: Path, dest: Path, hasHeader: Boolean): IO[Either[Throwable, Unit]] = {
    (for{
      d <- read(source, hasHeader)
      _ <- Html.htmlAsFile(d, dest)
    } yield ()).value
  }
}
