package ir.immanol.structure.instances.html

import ir.immanol.structure.htmlWriters.HtmlWriter
import ir.immanol.models.Info

/**
  * Here you can define instances for comverting an specific case class to its equivalent html.
  * Already there is one naive implementation for Info case class to a table row.
  */
object HtmlWriterInstances {

  implicit val InfoSimpleHtmlWriter = new HtmlWriter[Info] {
    override def write(value: Info): String =
      {
        val formatter = java.text.NumberFormat.getCurrencyInstance
        s"""<tr><td>${value.name}</td><td>${value.address}</td><td>${value.postcode}</td><td>${value.phone}</td><td>${formatter.format(value.creditLimit)}</td><td>${value.birthday}</td></tr>""".stripMargin
      }

    override def writeHeader: String =
      s"""<tr><th>NAME</th><th>ADDRESS</th><th>POSTCODE</th><th>PHONE</th><th>CREDITLIMIT</th><th>BIRTHDATE</th></tr>""".stripMargin
  }
}
