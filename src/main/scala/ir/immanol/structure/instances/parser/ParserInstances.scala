package ir.immanol.structure.instances.parser

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import ir.immanol.models.Info
import ir.immanol.structure.parsers.DataParser
import ir.immanol.utils.Util

import scala.util.Try
import scala.util.matching.Regex

/**
  * Here you can implement different parser to convert different string to different case classes
  * Our implementation are based on regex. To make it more testable we define an specific regex for each field.
  * And we use a utility method to combine them for parsing whole string line to case class.
  */
object ParserInstances{
  lazy val csvDateTimeFormatter: DateTimeFormatter =
    DateTimeFormatter
      .ofPattern("dd/MM/yyyy")
  lazy val prnDateTimeFormatter: DateTimeFormatter =
    DateTimeFormatter
      .ofPattern("yyyyMMdd")

  val nameRegex: Regex = """\"(\w+,\s*\w+)\"""".r
  val prnNameRegex: Regex = """(\w+,\s*\w+)""".r
  val addressRegex = """([\w\W]+\s\w+)""".r
  val postCodeRegex = """(\d{4}\s*[a-zA-Z]{2}|\d+)""".r
  val phoneRegex = """(\d+[\s|-]\d+|]|\+\d+\s\d+\s\d+)""".r
  val creditLimRegex = """(\d+\.?\d+?)""".r
  val birthDateRegex = """(\d{2}/\d{2}/\d{4})""".r
  val prnBirthDateRegex = """(\d{4}\d{2}\d{2})""".r

  val csvRegex = Util.combineRegex(nameRegex, addressRegex,
    postCodeRegex, phoneRegex, creditLimRegex, birthDateRegex)(",")
  val prnRegex = Util.combineRegex(prnNameRegex, addressRegex,
    postCodeRegex, phoneRegex, creditLimRegex, prnBirthDateRegex)("""\s+""")

  implicit val InfoCsvDataParser = new DataParser[Info] {
    def parse(value: String): Either[Throwable, Info] = {
      value match{
        case csvRegex(name, address, postCode, phone, creditLimit, birthDate) =>
          for{
            b <- Try(LocalDate.parse(birthDate, csvDateTimeFormatter)).toEither
            c <- Try(creditLimit.toDouble).toEither
          }yield Info(name, address, postCode, phone, c, b)
        case _ => Left(new Exception(s"can not parse line: ${value}"))
      }
    }
  }
  implicit val InfoPrnDataParser = new DataParser[Info] {
    def parse(value: String): Either[Throwable, Info] = {
      value match{
        case prnRegex(name, address, postCode, phone, creditLimit, birthDate) =>
          for{
            b <- Try(LocalDate.parse(birthDate, prnDateTimeFormatter)).toEither
            c <- Try(creditLimit.toDouble).toEither
          }yield Info(name, address, postCode, phone, c, b)
        case _ => Left(new Exception("can not parse line"))
      }
    }
  }
}
