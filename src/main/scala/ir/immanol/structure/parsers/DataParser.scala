package ir.immanol.structure.parsers

/**
  * this trait is in charge of converting a line of string to case class
  * @tparam A
  */
trait DataParser[A] {
  def parse(value: String): Either[Throwable, A]
}
