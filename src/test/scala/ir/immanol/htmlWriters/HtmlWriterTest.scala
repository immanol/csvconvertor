package ir.immanol.htmlWriters

import java.time.LocalDate

import ir.immanol.models.Info
import ir.immanol.structure.instances.parser.ParserInstances
import org.scalatest.{FlatSpec, Matchers}

class HtmlWriterTest extends FlatSpec with Matchers{

  behavior of "HtmlWriter"

  it should "generate a <tr>tds...</tr> structure for Info" in {
    import ir.immanol.structure.instances.html.HtmlWriterInstances._
    import ir.immanol.structure.syntaxs.html.HtmlSyntax._
    val info = Info("Iman", "Diemen", "1111 cx", "434332", 1000, LocalDate.parse("14/08/1981", ParserInstances.csvDateTimeFormatter))
    val html = info.toHtml
    html should startWith("<tr>")
    html should endWith("</tr>")
    html should  include("<td>434332</td>")
    html should  fullyMatch regex("""<tr>(<td>.+</td>){6}</tr>""")

  }
  it should "generate a some <tr>tds...</tr> structure for seq of Info" in {
    import ir.immanol.structure.instances.html.HtmlWriterInstances._
    import ir.immanol.structure.syntaxs.html.HtmlSyntax._
    val infos = Seq(
      Info("Iman", "Diemen", "1111 cx", "434332", 1000, LocalDate.parse("14/08/1981", ParserInstances.csvDateTimeFormatter)),
      Info("Maryam", "Diemen", "1111 cx", "23423423423", 4000, LocalDate.parse("21/09/1981", ParserInstances.csvDateTimeFormatter))
    )
    val html = infos.toHtml
    html should startWith("<tr>")
    html should endWith("</tr>")
    html should include("<td>1111 cx</td>")
  }

}
