package ir.immanol.parsers

import java.time.LocalDate

import ir.immanol.structure.instances.parser.ParserInstances
import ir.immanol.structure.interfaces.parser.Parser
import org.scalatest.{EitherValues, FlatSpec, Matchers}

class LineParserTest extends FlatSpec with Matchers with EitherValues{

  behavior of "LineParser"

  val valid_csv_case_1 = "\"Johnson, John\",Voorstraat 32,3122gg,020 3849381,10000,01/01/1987"
  val valid_prn_case_1 = "Johnson, John   Voorstraat 32         3122gg   020 3849381        1000000 19870101"

  it should s"parse a valid line ${valid_csv_case_1} of csv format to Info Model" in {
    import ParserInstances.InfoCsvDataParser
    val info = Parser.toModel(valid_csv_case_1)
    info should be ('right)
    info.right.value.name should be ("Johnson, John")
    info.right.value.address should be ("Voorstraat 32")
    info.right.value.postcode should be ("3122gg")
    info.right.value.phone should be ("020 3849381")
    info.right.value.creditLimit should be (10000)
    info.right.value.birthday should be (LocalDate.parse("01/01/1987", ParserInstances.csvDateTimeFormatter))
  }
  it should s"parse a valid line ${valid_prn_case_1} of prn format to Info Model" in {
    import ParserInstances.InfoPrnDataParser
    val info = Parser.toModel(valid_prn_case_1)
    info should be ('right)
    info.right.value.name should be ("Johnson, John")
    info.right.value.address should be ("Voorstraat 32")
    info.right.value.postcode should be ("3122gg")
    info.right.value.phone should be ("020 3849381")
    info.right.value.creditLimit should be (1000000)
    info.right.value.birthday should be (LocalDate.parse("19870101", ParserInstances.prnDateTimeFormatter))
  }

  it should "parse \"Johnson, John\" as name" in {
    "\"Johnson, John\"" match {
      case ParserInstances.nameRegex(value) => value should be("Johnson, John")
      case _ => fail(s"does not match")
    }
  }
  it should "parse Voorstraat 32 as addresss" in {
    "Voorstraat 32" match {
      case ParserInstances.addressRegex(value) => value should be("Voorstraat 32")
      case _ => fail(s"does not match")
    }
  }
  it should "parse Børkestraße 32 as addresss" in {
    "Børkestraße 32" match {
      case ParserInstances.addressRegex(value) => value should be("Børkestraße 32")
      case _ => fail(s"does not match")
    }
  }
  it should "parse 3122gg, 4532 AA and 87823 as PostCode" in {
    "3122gg" match {
      case ParserInstances.postCodeRegex(value) => value should be("3122gg")
      case _ => fail(s"does not match")
    }
    "4532 AA" match {
      case ParserInstances.postCodeRegex(value) => value should be("4532 AA")
      case _ => fail(s"does not match")
    }
    "87823" match {
      case ParserInstances.postCodeRegex(value) => value should be("87823")
      case _ => fail(s"does not match")
    }
  }
  it should "parse 020 3849381, 0313-398475 and +44 728 889838 as phone" in {
    "020 3849381" match {
      case ParserInstances.phoneRegex(value) => value should be("020 3849381")
      case _ => fail(s"does not match")
    }
    "0313-398475" match {
      case ParserInstances.phoneRegex(value) => value should be("0313-398475")
      case _ => fail(s"does not match")
    }
    "+44 728 889838" match {
      case ParserInstances.phoneRegex(value) => value should be("+44 728 889838")
      case _ => fail(s"does not match")
    }
  }
  it should "parse 10000 and 54.5 as addresss" in {
    "1000" match {
      case ParserInstances.creditLimRegex(value) => value should be("1000")
      case _ => fail(s"does not match")
    }
    "54.5" match {
      case ParserInstances.creditLimRegex(value) => value should be("54.5")
      case _ => fail(s"does not match")
    }
  }
  it should "parse 20/09/1999 as date" in {
    "20/09/1999" match {
      case ParserInstances.birthDateRegex(value) => value should be("20/09/1999")
      case _ => fail(s"does not match")
    }

  }

}
